stages:
  - build
  - test
  - security_test
  - publish

image: docker:20.10.20
services:
  - docker:20.10.20-dind

before_script:
  - export VERSION=$(grep version package.json | awk '{print $2}' | sed  's/[\"|,]//g' -)

# Build Docker image and upload artifacts
build:
  stage: build
  script:
    - docker build --no-cache -t $CI_PROJECT_NAME:$VERSION .
    - mkdir -p image
    - docker save $CI_PROJECT_NAME:$VERSION | gzip > image/app.tar.gz
  artifacts:
    name: "$CI_COMMIT_SHA"
    paths:
      - image
    expire_in: 30 mins
  tags:
    - demo

# Run unit tests
unit_test:
  stage: test
  script:
    - docker load -i image/app.tar.gz
    - docker run --rm $CI_PROJECT_NAME:$VERSION /bin/sh -c "npm install --include=dev && npm test"
  tags:
    - demo

# Run api tests
api_test:
  stage: test
  script:
    - docker load -i image/app.tar.gz
    - docker run --rm $CI_PROJECT_NAME:$VERSION /bin/sh -c "npm install --include=dev && npm run testapi"
  tags:
    - demo

# Run container scanning by Trivy
# Failed if with HIGH,CRITICAL vulnerabilities
container_scanning:
  stage: security_test
  script:
    - docker load -i image/app.tar.gz
    - docker run -v /var/run/docker.sock:/var/run/docker.sock aquasec/trivy image --exit-code 1 --severity HIGH,CRITICAL $CI_PROJECT_NAME:$VERSION
  tags:
    - demo

# Run SAST by Gitlab template
# tpl: https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml
include:
  - template: Security/SAST.gitlab-ci.yml

sast:
  stage: security_test  #override default stage
  artifacts:
    name: "$CI_COMMIT_SHA-$CI_JOB_NAME"
    paths:
      - gl-sast-report.json
    expire_in: 1 months
  tags:
    - demo

# Run secret detection
secret_detection:
  stage: security_test
  script:
    - docker run --rm -v ${PWD}:/code zricethezav/gitleaks:latest detect --no-git --source="code" -v
  tags:
    - demo

# Publish verified image
publish:
  stage: publish
  script:
    - docker load -i image/app.tar.gz
    - docker tag $CI_PROJECT_NAME:$VERSION $CI_REGISTRY_IMAGE:$VERSION
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker push $CI_REGISTRY_IMAGE:$VERSION
  only:
    - main
  tags:
    - demo